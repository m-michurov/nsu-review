import route
from control.authentication.authentication_controller import AuthenticationController
from data.accounts.user import User
from handle.request_handler import IRequestHandler
from view.view import IView
from view.view_factory import ViewFactory


class LoginHandler(IRequestHandler):

    def __init__(self, authentication_controller: AuthenticationController):
        self._authentication_controller: AuthenticationController = authentication_controller

    def handle_request(self, **params) -> IView:
        try:
            if 'username' not in params and 'password' not in params:
                if 'user' in params:
                    return ViewFactory.create_redirect(route.SEARCH_PAGE)

                return ViewFactory.create_login_page()

            username: str = params['username']
            password: str = params['password']

            if username == '':
                return ViewFactory.create_login_page('Введите имя пользователя')

            if password == '':
                return ViewFactory.create_login_page('Введите пароль')

            user: User = self._authentication_controller.authenticate(username, password)
            if not user:
                return ViewFactory.create_login_page(
                    'Пожалуйста, проверьте корректность введенных данных и попробуйте снова')

            return ViewFactory.add_cookies(ViewFactory.create_redirect(route.SEARCH_PAGE), 'user', username)
        except KeyError:
            return ViewFactory.create_login_page(
                'Пожалуйста, проверьте корректность введенных данных и попробуйте снова')
