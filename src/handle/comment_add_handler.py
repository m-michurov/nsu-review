import route
from control.teacher.teacher_info_controller import TeacherInfoController
from data.comments.comment import Comment
from data.teachers.teacher import Teacher
from handle.request_handler import IRequestHandler
from view.view import IView
from view.view_factory import ViewFactory


class CommentAddHandler(IRequestHandler):

    def __init__(self, teacher_info_controller: TeacherInfoController):
        self._teacher_info_controller: TeacherInfoController = teacher_info_controller

    def handle_request(self, **params) -> IView:
        try:
            teacher_id: int = int(params['teacher_id'])
            user: str = params['user']
            text: str = params['comment']

            teacher: Teacher = self._teacher_info_controller.get_by_id(teacher_id)
            if not teacher:
                return ViewFactory.create_bad_request_page()

            new_comment: Comment = self._teacher_info_controller.add_comment(teacher_id, user, text)
            if not new_comment:
                return ViewFactory.create_bad_request_page()
            return ViewFactory.create_redirect(
                '{}/{}#comment_{}'.format(route.TEACHER_PAGE, teacher_id, new_comment.get_id()))
        except KeyError:
            return ViewFactory.create_bad_request_page()
