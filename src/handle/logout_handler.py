import route
from control.authentication.authentication_controller import AuthenticationController
from handle.request_handler import IRequestHandler
from view.view import IView
from view.view_factory import ViewFactory


class LogoutHandler(IRequestHandler):

    def __init__(self, authentication_controller: AuthenticationController):
        self._authentication_controller: AuthenticationController = authentication_controller

    def handle_request(self, **params) -> IView:
        try:
            username: str = params['user']
            self._authentication_controller.logout(username)
            return ViewFactory.add_cookies(ViewFactory.create_redirect(route.LOGIN_FORM), 'user', '', expires=0)
        except KeyError:
            return ViewFactory.create_bad_request_page()
