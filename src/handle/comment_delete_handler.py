import route
from control.authorization.authorization_controller import AuthorizationController, AuthorizationResult
from control.teacher.teacher_info_controller import TeacherInfoController
from data.comments.comment import Comment
from data.teachers.teacher import Teacher
from handle.request_handler import IRequestHandler
from view.view import IView
from view.view_factory import ViewFactory


class CommentDeleteHandler(IRequestHandler):

    def __init__(
            self,
            teacher_info_controller: TeacherInfoController,
            authorization_controller: AuthorizationController):
        self._teacher_info_controller: TeacherInfoController = teacher_info_controller
        self._authorization_controller: AuthorizationController = authorization_controller

    def handle_request(self, **params) -> IView:
        try:
            comment_id: int = int(params['comment_id'])
            teacher_id: int = int(params['teacher_id'])

            username: str = params['user']
            comment: Comment = self._teacher_info_controller.get_comment(comment_id)
            if not comment:
                return ViewFactory.create_bad_request_page()

            action: str = AuthorizationController.DELETE_OWN_COMMENT if username == comment.get_author() \
                else AuthorizationController.DELETE_COMMENT

            authorization_result: AuthorizationResult = self._authorization_controller.authorize(username, action)
            if not authorization_result.success():
                return ViewFactory.create_bad_request_page()

            self._teacher_info_controller.remove_comment(comment_id)

            teacher: Teacher = self._teacher_info_controller.get_by_id(teacher_id)
            if not teacher:
                return ViewFactory.create_not_found_page()

            return ViewFactory.create_redirect('{}/{}'.format(route.TEACHER_PAGE, teacher_id))
        except KeyError:
            return ViewFactory.create_bad_request_page()
