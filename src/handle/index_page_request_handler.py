from handle.request_handler import IRequestHandler
from view.view import IView
from view.view_factory import ViewFactory


class IndexPageRequestHandler(IRequestHandler):

    def handle_request(self, **_) -> IView:
        return ViewFactory.create_index_page()
