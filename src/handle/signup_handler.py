import route
from control.account.account_controller import AccountController, AccountOperationResult
from control.authentication.authentication_controller import AuthenticationController
from data.accounts.user import User
from handle.request_handler import IRequestHandler
from view.view import IView
from view.view_factory import ViewFactory


class SignupHandler(IRequestHandler):
    _MIN_PASSWORD_LENGTH: int = 8

    def __init__(
            self,
            authentication_controller: AuthenticationController,
            account_controller: AccountController):
        self._authentication_controller: AuthenticationController = authentication_controller
        self._account_controller: AccountController = account_controller

    def handle_request(self, **params) -> IView:
        try:
            if 'username' not in params and 'password' not in params:
                return ViewFactory.create_signup_page()

            username: str = params['username']
            password: str = params['password']

            if username == '':
                return ViewFactory.create_signup_page('Введите имя пользователя')

            if len(password) < SignupHandler._MIN_PASSWORD_LENGTH:
                return ViewFactory.create_signup_page(
                    'Введите пароль не короче {} символов'.format(SignupHandler._MIN_PASSWORD_LENGTH))

            user: User = self._account_controller.get_user(username)
            if user:
                return ViewFactory.create_signup_page('Имя пользователя уже занято')

            result: AccountOperationResult = self._account_controller.create_account(username, password)
            if not result.success():
                return ViewFactory.create_signup_page('Не удалось создать аккаунт: {}'.format(result.get_err_message()))

            return ViewFactory.add_cookies(ViewFactory.create_redirect(route.SEARCH_PAGE), 'user', username)
        except KeyError:
            return ViewFactory.create_signup_page(
                'Пожалуйста, проверьте корректность введенных данных и попробуйте снова')
