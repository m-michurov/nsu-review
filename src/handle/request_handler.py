from view.view import IView


class IRequestHandler:

    def handle_request(self, **params) -> IView:
        raise NotImplementedError
