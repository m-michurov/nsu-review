import route
from control.account.account_controller import AccountController
from data.accounts.user import User
from handle.request_handler import IRequestHandler
from view.view import IView
from view.view_factory import ViewFactory


class UserPageRequestHandler(IRequestHandler):

    def __init__(self, account_controller: AccountController):
        self._account_controller: AccountController = account_controller

    def handle_request(self, **params) -> IView:
        try:
            if 'user' not in params:
                return ViewFactory.create_redirect(route.LOGIN_FORM)

            viewer: str = params['user']
            viewer_account: User = self._account_controller.get_user(viewer)
            if not viewer_account:
                return ViewFactory.create_bad_request_page()

            username: str = params['username']
            user = self._account_controller.get_user(username)
            if not user:
                return ViewFactory.create_not_found_page()

            return ViewFactory.create_user_page(user, params['user'], admin=viewer_account.get_acl().is_admin())
        except KeyError:
            return ViewFactory.create_bad_request_page()
