from typing import Optional

import route
from control.authorization.authorization_controller import AuthorizationController, AuthorizationResult
from control.teacher.teacher_info_controller import TeacherInfoController
from data.teachers.teacher import Teacher
from handle.request_handler import IRequestHandler
from view.view import IView
from view.view_factory import ViewFactory


class TeacherPageCreateHandler(IRequestHandler):

    def __init__(
            self,
            teacher_info_controller: TeacherInfoController,
            authorization_controller: AuthorizationController):
        self._teacher_info_controller: TeacherInfoController = teacher_info_controller
        self._authorization_controller: AuthorizationController = authorization_controller

    def handle_request(self, **params) -> IView:
        try:
            username: str = params['user']

            authorization_result: AuthorizationResult = self._authorization_controller.authorize(
                username, AuthorizationController.CREATE_TEACHER_PAGE)
            if not authorization_result.success():
                return ViewFactory.create_bad_request_page()

            if 'name' not in params \
                    and 'academic_rank' not in params \
                    and 'image_link' not in params \
                    and 'courses' not in params \
                    and 'bio' not in params:
                return ViewFactory.create_teacher_page_create_form(username)

            name: str = params['name']
            academic_rank: str = params['academic_rank']
            image_link: str = params['image_link']
            courses: str = params['courses']
            bio: str = params['bio']

            teacher: Optional[Teacher] = self._teacher_info_controller.create_teacher(
                name, academic_rank, bio, image_link, list(map(lambda it: it.strip(), courses.split(';'))))
            if not teacher:
                return ViewFactory.create_teacher_page_create_form(
                    username, 'Не удалось создать страницу преподавателя. Возможно, такая страница уже существует?')

            return ViewFactory.create_redirect('{}/{}'.format(route.TEACHER_PAGE, teacher.get_id()))
        except KeyError:
            return ViewFactory.create_bad_request_page()
