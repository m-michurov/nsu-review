import route
from control.account.account_controller import AccountController
from control.search.search_controller import SearchController
from data.accounts.user import User
from handle.request_handler import IRequestHandler
from view.view import IView
from view.view_factory import ViewFactory


class SearchHandler(IRequestHandler):

    def __init__(
            self,
            search_controller: SearchController,
            account_controller: AccountController):
        self._search_controller: SearchController = search_controller
        self._account_controller: AccountController = account_controller

    def handle_request(self, **params) -> IView:
        try:
            if 'user' not in params:
                return ViewFactory.create_redirect(route.LOGIN_FORM)

            username: str = params['user']
            user: User = self._account_controller.get_user(username)
            if not user:
                return ViewFactory.create_bad_request_page()

            if 'teacher_name' not in params:
                return ViewFactory.create_search_page(username, admin=user.get_acl().is_admin())

            teacher_name: str = params['teacher_name']
            if teacher_name == '':
                return ViewFactory.create_search_page(username, 'Введите поисковый запрос', user.get_acl().is_admin())

            search_results = self._search_controller.find({'name': teacher_name})
            return ViewFactory.create_search_results_page(
                username, teacher_name, search_results, user.get_acl().is_admin())
        except KeyError:
            return ViewFactory.create_bad_request_page()
