from control.authorization.authorization_controller import AuthorizationController, AuthorizationResult
from handle.request_handler import IRequestHandler
from view.view import IView
from view.view_factory import ViewFactory


class AdminControlPanelRequestHandler(IRequestHandler):

    def __init__(self, authorization_controller: AuthorizationController):
        self._authorization_controller: AuthorizationController = authorization_controller

    def handle_request(self, **params) -> IView:
        try:
            username: str = params['user']

            authorization_result: AuthorizationResult = self._authorization_controller.authorize(
                username, AuthorizationController.OPEN_CONTROL_PANEL)
            if not authorization_result.success():
                return ViewFactory.create_bad_request_page()

            return ViewFactory.create_admin_control_panel(username)
        except KeyError:
            return ViewFactory.create_bad_request_page()
