from typing import List

import route
from control.account.account_controller import AccountController
from control.teacher.teacher_info_controller import TeacherInfoController
from data.accounts.user import User
from data.comments.comment import Comment
from data.teachers.teacher import Teacher
from handle.request_handler import IRequestHandler
from view.view import IView
from view.view_factory import ViewFactory


class TeacherPageRequestHandler(IRequestHandler):

    def __init__(
            self,
            teacher_info_controller: TeacherInfoController,
            account_controller: AccountController):
        self._teacher_info_controller: TeacherInfoController = teacher_info_controller
        self._account_controller: AccountController = account_controller

    def handle_request(self, **params) -> IView:
        try:
            if 'user' not in params:
                return ViewFactory.create_redirect(route.LOGIN_FORM)

            teacher_id: int = int(params['teacher_id'])
            teacher: Teacher = self._teacher_info_controller.get_by_id(teacher_id)
            if not teacher:
                return ViewFactory.create_not_found_page()
            username: str = params['user']
            user: User = self._account_controller.get_user(username)
            if not user:
                return ViewFactory.create_bad_request_page()

            comments: List[Comment] = self._teacher_info_controller.get_comments(teacher_id)
            return ViewFactory.create_teacher_page(teacher, comments, username, admin=user.get_acl().is_admin())
        except KeyError:
            return ViewFactory.create_bad_request_page()
