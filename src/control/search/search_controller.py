from typing import List, Dict

from data.teachers.teacher import Teacher
from data.teachers.teachers import TeachersDB


class SearchController:

    def __init__(self, teachers: TeachersDB):
        self._teachers: TeachersDB = teachers

    def find(self, queries: Dict[str, str]) -> List[Teacher]:
        if 'name' in queries:
            return self._teachers.find_by_name(queries['name'])

        return []
