from typing import Optional

from data.accounts.accounts import AccountsDB
from data.accounts.acl import AccessControlList
from data.accounts.user import User
from data.auth.passwords import PasswordsDB


class AccountOperationResult:

    def __init__(self, success: bool, err: Optional[str]):
        self._success: bool = success
        self._err: Optional[str] = err

    def success(self) -> bool:
        return self._success

    def get_err_message(self) -> Optional[str]:
        return self._err


class AccountController:

    def __init__(self, accounts_db: AccountsDB, passwords_db: PasswordsDB):
        self._users: AccountsDB = accounts_db
        self._passwords: PasswordsDB = passwords_db

    def get_user(self, username: str) -> Optional[User]:
        return self._users.get_user(username)

    def create_account(self, username: str, password: str) -> AccountOperationResult:
        if self._users.get_user(username):
            return AccountOperationResult(False, 'имя пользователя занято')

        self._users.create_user(username, PasswordsDB.hash(password), AccessControlList(admin=False, visible=True))
        self._passwords.set_password(username, password)
        return AccountOperationResult(True, None)
