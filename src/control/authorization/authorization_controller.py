from typing import Optional, List

from data.accounts.accounts import AccountsDB
from data.accounts.user import User


class AuthorizationResult:

    def __init__(self, success: bool, err: Optional[str]):
        self._success: bool = success
        self._err: Optional[str] = err

    def success(self) -> bool:
        return self._success

    def get_err_message(self) -> Optional[str]:
        return self._err


class AuthorizationController:
    OPEN_CONTROL_PANEL: str = 'open_admin_control_panel'
    CREATE_TEACHER_PAGE: str = 'create_teacher_page'
    DELETE_COMMENT: str = 'delete_comment'
    DELETE_OWN_COMMENT: str = 'delete_own_comment'

    _ADMIN_PERMITTED_ACTIONS: List[str] = [
        OPEN_CONTROL_PANEL,
        CREATE_TEACHER_PAGE,
        DELETE_OWN_COMMENT,
        DELETE_COMMENT
    ]

    _NORMAL_USER_PERMITTED_ACTIONS: List[str] = [
        DELETE_OWN_COMMENT
    ]

    def __init__(self, accounts_db: AccountsDB):
        self._users: AccountsDB = accounts_db

    def authorize(self, user_token: str, action: str) -> AuthorizationResult:
        username: str = user_token
        user: User = self._users.get_user(username)

        success: bool

        if user.get_acl().is_admin():
            success = action in AuthorizationController._ADMIN_PERMITTED_ACTIONS
        else:
            success = action in AuthorizationController._NORMAL_USER_PERMITTED_ACTIONS

        err: str = 'действие запрещено' if not success else None
        return AuthorizationResult(success, err)
