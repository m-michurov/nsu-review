from typing import Optional

from data.accounts.accounts import AccountsDB
from data.accounts.user import User
from data.auth.passwords import PasswordsDB


class AuthenticationController:

    def __init__(self, accounts_db: AccountsDB, passwords_db: PasswordsDB):
        self._users: AccountsDB = accounts_db
        self._passwords: PasswordsDB = passwords_db

    def authenticate(self, username: str, password: str) -> Optional[User]:
        user: Optional[User] = self._users.get_user(username)
        if not user:
            return None

        if PasswordsDB.hash(password) != user.get_password_hash():
            return None

        if self._passwords.check_password(username, password):
            return user

        return None

    def logout(self, token: str) -> None:
        pass
