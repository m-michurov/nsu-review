from typing import Optional, List

from data.accounts.accounts import AccountsDB
from data.accounts.user import User
from data.comments.comment import Comment
from data.comments.comments import CommentsDB
from data.teachers.teacher import Teacher
from data.teachers.teachers import TeachersDB


class TeacherInfoController:

    def __init__(
            self,
            users: AccountsDB,
            teachers: TeachersDB,
            comments: CommentsDB):
        self._users: AccountsDB = users
        self._teachers: TeachersDB = teachers
        self._comments: CommentsDB = comments

    def create_teacher(self, name: str, rank: str, bio: str, image_link: str, courses: List[str]) -> Optional[Teacher]:
        return self._teachers.create_teacher(name, rank, bio, image_link, courses)

    def get_by_id(self, teacher_id: int) -> Optional[Teacher]:
        return self._teachers.get_by_id(teacher_id)

    def add_comment(self, teacher_id: int, author: str, text: str) -> Optional[Comment]:
        user: User = self._users.get_user(author)
        if not user:
            return None

        if not self._teachers.get_by_id(teacher_id):
            return None
        comment: Comment = self._comments.create_comment(teacher_id, user.get_name(), text)
        user.increment_comments_count()
        self._users.update_user(user)
        return comment

    def get_comments(self, teacher_id: int) -> List[Comment]:
        return self._comments.get_comments_about_teacher(teacher_id)

    def get_comment(self, comment_id: int) -> Optional[Comment]:
        return self._comments.get_comment(comment_id)

    def remove_comment(self, comment_id: int) -> None:
        author_name: Optional[str] = self._comments.delete_comment(comment_id)
        if not author_name:
            return

        author: Optional[User] = self._users.get_user(author_name)
        if not author:
            print("!!!! author {} not found".format(author_name))
            return

        author.decrement_comments_count()
        self._users.update_user(author)
