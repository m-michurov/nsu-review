import route

from control.account.account_controller import AccountController
from control.authentication.authentication_controller import AuthenticationController
from control.authorization.authorization_controller import AuthorizationController
from control.search.search_controller import SearchController
from control.teacher.teacher_info_controller import TeacherInfoController

from data.accounts.accounts import AccountsDB
from data.auth.passwords import PasswordsDB
from data.comments.comments import CommentsDB
from data.teachers.teachers import TeachersDB

from handle.admin_control_panel_request_handler import AdminControlPanelRequestHandler
from handle.comment_delete_handler import CommentDeleteHandler
from handle.index_page_request_handler import IndexPageRequestHandler
from handle.login_handler import LoginHandler
from handle.logout_handler import LogoutHandler
from handle.search_handler import SearchHandler
from handle.signup_handler import SignupHandler
from handle.teacher_page_create_handler import TeacherPageCreateHandler
from handle.teacher_page_request_handler import TeacherPageRequestHandler
from handle.comment_add_handler import CommentAddHandler
from handle.user_page_request_handler import UserPageRequestHandler
from request_router import RequestRouter


def create_request_router() -> RequestRouter:
    accounts_db = AccountsDB()
    passwords_db = PasswordsDB()
    teachers_db = TeachersDB()
    comments_db = CommentsDB()

    account_controller = AccountController(accounts_db, passwords_db)
    authentication_controller = AuthenticationController(accounts_db, passwords_db)
    authorization_controller = AuthorizationController(accounts_db)

    teacher_info_controller = TeacherInfoController(accounts_db, teachers_db, comments_db)
    search_controller = SearchController(teachers_db)

    return RequestRouter({
        route.ADMIN_CONTROL_PANEL: AdminControlPanelRequestHandler(authorization_controller),
        route.INDEX_PAGE: IndexPageRequestHandler(),
        route.TEACHER_PAGE: TeacherPageRequestHandler(teacher_info_controller, account_controller),
        route.TEACHER_PAGE_CREATE_FORM: TeacherPageCreateHandler(teacher_info_controller, authorization_controller),
        route.POST_COMMENT: CommentAddHandler(teacher_info_controller),
        route.DELETE_COMMENT: CommentDeleteHandler(teacher_info_controller, authorization_controller),
        route.USER_PAGE: UserPageRequestHandler(account_controller),
        route.ACCOUNT_CREATE_FORM: SignupHandler(authentication_controller, account_controller),
        route.LOGIN_FORM: LoginHandler(authentication_controller),
        route.LOGOUT: LogoutHandler(authentication_controller),
        route.SEARCH_PAGE: SearchHandler(search_controller, account_controller)
    })
