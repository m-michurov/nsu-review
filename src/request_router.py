from typing import Dict

from handle.request_handler import IRequestHandler
from view.view import IView
from view.view_factory import ViewFactory


class RequestRouter:

    def __init__(self, handlers: Dict[str, IRequestHandler]):
        self._handlers: Dict[str, IRequestHandler] = handlers

    def handle_request(self, endpoint: str, **params) -> IView:
        if endpoint not in self._handlers:
            return ViewFactory.create_service_unavailable_page()

        return self._handlers[endpoint].handle_request(**params)
