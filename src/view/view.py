

class IView:

    def render(self) -> str:
        raise NotImplementedError
