from flask import render_template

from view.view import IView


class BadRequestPage(IView):
    _TEMPLATE_NAME: str = 'bad_request.html'

    def render(self) -> str:
        return render_template(BadRequestPage._TEMPLATE_NAME)
