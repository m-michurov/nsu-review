from datetime import datetime

from flask import render_template

from data.accounts.user import User
from view.view import IView


class UserPage(IView):
    _TEMPLATE_NAME: str = 'user.html'

    def __init__(self, user: User, viewer: str, admin: bool = False):
        self._user: User = user
        self._viewer: str = viewer
        self._admin: bool = admin

    def render(self) -> str:
        user: dict = {
            **self._user.__dict__(),
            'role': 'Администратор' if self._user.get_acl().is_admin() else 'Обычный пользователь',
            'signup_date': datetime.utcfromtimestamp(self._user.get_create_time()).strftime('%d.%m.%Y %H:%M')
        }
        return render_template(
            UserPage._TEMPLATE_NAME,
            user=user,
            hidden=False,
            viewer={'name': self._viewer},
            admin=self._admin)
