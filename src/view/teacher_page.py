from datetime import datetime
from typing import List

from flask import render_template

from data.accounts.rate import Rate
from data.comments.comment import Comment
from data.teachers.teacher import Teacher
from view.view import IView


class TeacherPage(IView):
    _TEMPLATE_NAME: str = 'teacher.html'

    def __init__(
            self,
            teacher: Teacher,
            comments: List[Comment],
            viewer: str,
            rates: List[Rate] = None,
            admin: bool = False):
        if rates is None:
            rates = []
        self._teacher: Teacher = teacher
        self._comments: List[Comment] = comments
        self._viewer: str = viewer
        self._rates: List[Rate] = rates
        self._is_admin: bool = admin

    def render(self) -> str:
        teacher_info: dict = {
            **self._teacher.__dict__(),
            'rating': str(self._teacher.get_rating())
        }
        comments: List[dict] = list(map(
            lambda it: {
                **it.__dict__(),
                'rating': str(it.get_rating()),
                'time': datetime.utcfromtimestamp(it.get_create_time()).strftime('%d.%m.%Y %H:%M')
            },
            self._comments))

        return render_template(
            TeacherPage._TEMPLATE_NAME,
            teacher=teacher_info,
            comments=comments,
            viewer={'name': self._viewer},
            admin=self._is_admin)
