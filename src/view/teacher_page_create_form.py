from typing import Optional

from flask import render_template

from view.view import IView


class TeacherPageCreateForm(IView):
    _TEMPLATE_NAME: str = 'create_teacher.html'

    def __init__(self, viewer: str, flash_message: Optional[str] = None):
        self._viewer: str = viewer
        self._flash_message: Optional[str] = flash_message

    def render(self) -> str:
        return render_template(
            TeacherPageCreateForm._TEMPLATE_NAME,
            admin=True,
            viewer={'name': self._viewer},
            message=self._flash_message)
