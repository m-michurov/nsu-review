from flask import render_template

from view.view import IView


class IndexPage(IView):
    _TEMPLATE_NAME: str = 'index.html'

    def render(self) -> str:
        return render_template(IndexPage._TEMPLATE_NAME)
