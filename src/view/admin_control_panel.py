from flask import render_template

from view.view import IView


class AdminControlPanel(IView):
    _TEMPLATE_NAME: str = 'admin.html'

    def __init__(self, viewer: str):
        self._viewer: str = viewer

    def render(self) -> str:
        return render_template(AdminControlPanel._TEMPLATE_NAME, admin=True, viewer={'name': self._viewer})
