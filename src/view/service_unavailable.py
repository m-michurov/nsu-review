from flask import render_template

from view.view import IView


class ServiceUnavailablePage(IView):
    _TEMPLATE_NAME: str = 'service_unavailable.html'

    def render(self) -> str:
        return render_template(ServiceUnavailablePage._TEMPLATE_NAME)
