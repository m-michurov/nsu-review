from flask import render_template

from view.view import IView


class NotFoundPage(IView):
    _TEMPLATE_NAME: str = 'not_found.html'

    def render(self) -> str:
        return render_template(NotFoundPage._TEMPLATE_NAME)
