from typing import Optional

from flask import render_template

from view.view import IView


class SearchPage(IView):
    _TEMPLATE_NAME: str = 'search.html'

    def __init__(self, viewer: str, flash_message: Optional[str] = None, admin: bool = False):
        self._viewer: str = viewer
        self._flash_message: Optional[str] = flash_message
        self._admin: bool = admin

    def render(self) -> str:
        return render_template(
            SearchPage._TEMPLATE_NAME,
            viewer={'name': self._viewer},
            message=self._flash_message,
            admin=self._admin)
