from typing import List

from flask import render_template

from data.teachers.teacher import Teacher
from view.view import IView


class SearchResultsPage(IView):
    _TEMPLATE_NAME: str = 'search_results.html'

    def __init__(self, viewer: str, query: str, results: List[Teacher], admin: bool = False):
        self._viewer: str = viewer
        self._query: str = query
        self._results: List[Teacher] = results
        self._admin: bool = admin

    def render(self) -> str:
        if not self._results:
            return render_template(
                SearchResultsPage._TEMPLATE_NAME,
                viewer={'name': self._viewer},
                not_found=True,
                query=self._query)

        results: List[dict] = list(map(
            lambda it: it.__dict__(),
            self._results
        ))

        return render_template(
            SearchResultsPage._TEMPLATE_NAME,
            viewer={'name': self._viewer},
            results=results,
            query=self._query,
            admin=self._admin)
