from typing import Optional

from flask import render_template

from view.view import IView


class LoginPage(IView):
    _TEMPLATE_NAME: str = 'login.html'

    def __init__(self, flash_message: Optional[str] = None):
        self._flash_message: Optional[str] = flash_message

    def render(self) -> str:
        return render_template(LoginPage._TEMPLATE_NAME, message=self._flash_message)
