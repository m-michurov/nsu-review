from typing import List, Optional

from flask import make_response
from werkzeug.utils import redirect

from data.accounts.rate import Rate
from data.accounts.user import User
from data.comments.comment import Comment
from data.teachers.teacher import Teacher

from view.admin_control_panel import AdminControlPanel
from view.bad_request import BadRequestPage
from view.index_page import IndexPage
from view.login_page import LoginPage
from view.not_found import NotFoundPage
from view.search_page import SearchPage
from view.search_results_page import SearchResultsPage
from view.create_account_form import CreateAccountForm
from view.teacher_page import TeacherPage
from view.teacher_page_create_form import TeacherPageCreateForm
from view.user_page import UserPage
from view.service_unavailable import ServiceUnavailablePage
from view.view import IView


class _Redirect(IView):

    def __init__(self, to: str):
        self._to: str = to

    def render(self) -> str:
        return redirect(self._to)


class _WithCookies(IView):

    def __init__(self, view: IView, key: str, value: str, expires: Optional[int] = None):
        self._view: IView = view
        self._key: str = key
        self._value: str = value
        self._expires: Optional[int] = expires

    def render(self) -> str:
        response = make_response(self._view.render())
        if self._expires is not None:
            response.set_cookie(self._key, self._value, expires=self._expires)
        else:
            response.set_cookie(self._key, self._value)
        return response


class ViewFactory:

    @staticmethod
    def add_cookies(view: IView, key: str, value: str, expires: Optional[int] = None) -> IView:
        return _WithCookies(view, key, value, expires)

    @staticmethod
    def create_index_page() -> IView:
        return IndexPage()

    @staticmethod
    def create_teacher_page(
            teacher: Teacher,
            comments: List[Comment],
            viewer: str,
            rates: List[Rate] = None,
            admin: bool = False) -> IView:
        return TeacherPage(teacher, comments, viewer, rates, admin)

    @staticmethod
    def create_user_page(user: User, viewer: str, admin: bool = False) -> IView:
        return UserPage(user, viewer, admin)

    @staticmethod
    def create_not_found_page() -> IView:
        return NotFoundPage()

    @staticmethod
    def create_service_unavailable_page() -> IView:
        return ServiceUnavailablePage()

    @staticmethod
    def create_bad_request_page() -> IView:
        return BadRequestPage()

    @staticmethod
    def create_redirect(to: str) -> IView:
        return _Redirect(to)

    @staticmethod
    def create_login_page(message: Optional[str] = None) -> IView:
        return LoginPage(message)

    @staticmethod
    def create_signup_page(message: Optional[str] = None) -> IView:
        return CreateAccountForm(message)

    @staticmethod
    def create_search_page(viewer: str, message: Optional[str] = None, admin: bool = False) -> IView:
        return SearchPage(viewer, message, admin)

    @staticmethod
    def create_search_results_page(viewer: str, query: str, results: List[Teacher], admin: bool = False) -> IView:
        return SearchResultsPage(viewer, query, results, admin)

    @staticmethod
    def create_admin_control_panel(viewer: str) -> IView:
        return AdminControlPanel(viewer)

    @staticmethod
    def create_teacher_page_create_form(viewer: str, message: Optional[str] = None) -> IView:
        return TeacherPageCreateForm(viewer, message)
