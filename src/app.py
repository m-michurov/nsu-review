from flask import Flask, request

from request_router import RequestRouter
from view.view import IView
from init import create_request_router

import route

app = Flask(__name__)
app.secret_key = 'something unique and special'

request_router: RequestRouter = create_request_router()


@app.route(route.ROOT_PAGE)
@app.route(route.INDEX_PAGE)
def index():
    view: IView = request_router.handle_request(route.INDEX_PAGE)
    return view.render()


@app.route(route.SEARCH_PAGE)
def search():
    view: IView = request_router.handle_request(route.SEARCH_PAGE, **request.values, **request.cookies)
    return view.render()


@app.route(route.USER_PAGE + '/<username>')
def user(username: str):
    view: IView = request_router.handle_request(route.USER_PAGE, username=username, **request.cookies)
    return view.render()


@app.route(route.ACCOUNT_CREATE_FORM, methods=['POST', 'GET'])
def signup():
    view: IView = request_router.handle_request(route.ACCOUNT_CREATE_FORM, **request.values, **request.cookies)
    return view.render()


@app.route(route.LOGIN_FORM, methods=['POST', 'GET'])
def login():
    view: IView = request_router.handle_request(route.LOGIN_FORM, **request.values, **request.cookies)
    return view.render()


@app.route(route.LOGOUT)
def logout():
    view: IView = request_router.handle_request(route.LOGOUT, **request.cookies)
    return view.render()


@app.route(route.TEACHER_PAGE_CREATE_FORM, methods=['POST', 'GET'])
def create_teacher_page():
    view: IView = request_router.handle_request(route.TEACHER_PAGE_CREATE_FORM, **request.values, **request.cookies)
    return view.render()


@app.route(route.TEACHER_PAGE + '/<teacher_id>')
def teacher(teacher_id: str):
    view: IView = request_router.handle_request(route.TEACHER_PAGE, teacher_id=teacher_id, **request.cookies)
    return view.render()


@app.route(route.POST_COMMENT, methods=['POST'])
def post_comment():
    view: IView = request_router.handle_request(route.POST_COMMENT, **request.values, **request.cookies)
    return view.render()


@app.route(route.DELETE_COMMENT)
def delete_comment():
    view: IView = request_router.handle_request(route.DELETE_COMMENT, **request.values, **request.cookies)
    return view.render()


@app.route(route.ADMIN_CONTROL_PANEL)
def admin_control_panel():
    view: IView = request_router.handle_request(route.ADMIN_CONTROL_PANEL, **request.values, **request.cookies)
    return view.render()
