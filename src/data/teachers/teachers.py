import json
import os
from typing import List, Dict, Optional

from data.teachers.teacher import Teacher


class TeachersDB:
    _DATA_FILE_NAME: str = 'db/teachers.json'

    @staticmethod
    def _load_teacher(t: dict) -> Teacher:
        return Teacher(
            _id=t['id'],
            name=t['name'],
            academic_rank=t['academic_rank'],
            bio=t['bio'],
            image_link=t['image_link'],
            courses=t['courses'],
            total_rating=t['total_rating'],
            rates_count=t['rates_count'])

    def __init__(self):
        teachers: List[dict]
        try:
            with open(TeachersDB._DATA_FILE_NAME, 'r') as teachers_file:
                teachers = json.load(teachers_file)
        except FileNotFoundError:
            os.makedirs(os.path.dirname(TeachersDB._DATA_FILE_NAME), exist_ok=True)
            with open(TeachersDB._DATA_FILE_NAME, 'w') as teachers_file:
                teachers_file.write('[]')
            teachers = []

        self._teachers: Dict[int, Teacher] = {}
        max_id: int = 0

        for teacher_json in teachers:
            teacher: Teacher = TeachersDB._load_teacher(teacher_json)
            self._teachers[teacher.get_id()] = teacher
            if teacher.get_id() > max_id:
                max_id = teacher.get_id()

        self._next_id = max_id + 1

    def get_by_id(self, _id: int) -> Optional[Teacher]:
        """
        returns a copy
        """
        if _id not in self._teachers:
            return None

        return TeachersDB._load_teacher(self._teachers[_id].__dict__())

    def create_teacher(
            self,
            name: str,
            academic_rank: str,
            bio: str,
            image_link: str,
            courses: List[str]) -> Optional[Teacher]:
        _id: int = self._next_id
        self._next_id += 1

        teacher = Teacher(
            _id=_id,
            name=name,
            academic_rank=academic_rank,
            bio=bio,
            image_link=image_link,
            courses=courses)

        for teacher_id in self._teachers:
            if self._teachers[teacher_id] == teacher:
                return None

        self._teachers[teacher.get_id()] = teacher
        self._store()

        return self.get_by_id(teacher.get_id())

    def update_teacher(self, teacher: Teacher) -> None:
        self._teachers[teacher.get_id()] = teacher
        self._store()

    def find_by_name(self, name: str) -> List[Teacher]:
        name = name.lower()
        result: List[Teacher] = []

        for teacher_id in self._teachers:
            teacher = self._teachers[teacher_id]
            teacher_name: str = teacher.get_name().lower()

            if name in teacher_name or teacher_name in name:
                result.append(self.get_by_id(teacher.get_id()))

        return result

    def _store(self) -> None:
        os.makedirs(os.path.dirname(TeachersDB._DATA_FILE_NAME), exist_ok=True)
        with open(TeachersDB._DATA_FILE_NAME, 'w') as teachers_file:
            json.dump(
                obj=list(map(lambda teacher_id: self._teachers[teacher_id].__dict__(), self._teachers)),
                fp=teachers_file)
