from data.teachers.teacher import Teacher
from data.teachers.teachers import TeachersDB


def run() -> None:
    db = TeachersDB()
    for t in db.find_by_name('Ипполитов'):
        t.add_rate(5)
        t.add_rate(5)
        db.update_teacher(t)

    for t in db.find_by_name('Иртегов'):
        t.add_rate(0)
        t.add_rate(1)
        db.update_teacher(t)

    for t in db.find_by_name(''):
        print(t)
        print(t.get_rating())


if __name__ == '__main__':
    run()
