from typing import List


class Teacher:

    def __init__(
            self,
            _id: int,
            name: str,
            academic_rank: str,
            bio: str,
            image_link: str,
            courses: List[str],
            total_rating: int = 0,
            rates_count: int = 0):
        self._id: int = _id
        self._name: str = name
        self._academic_rank: str = academic_rank
        self._bio: str = bio
        self._image_link: str = image_link
        self._courses: List[str] = courses
        self._total_rating: int = total_rating
        self._rates_count: int = rates_count

    def add_rate(self, rate: int) -> None:
        self._total_rating += rate
        self._rates_count += 1

    def get_rating(self) -> float:
        return 0 if self._rates_count == 0 else self._total_rating / self._rates_count

    def get_name(self) -> str:
        return self._name

    def get_id(self) -> int:
        return self._id

    def get_academic_rank(self) -> str:
        return self._academic_rank

    def get_bio(self) -> str:
        return self._bio

    def get_courses(self) -> List[str]:
        return self._courses

    def get_image_link(self) -> str:
        return self._image_link

    def __dict__(self) -> dict:
        return {
            'id': self._id,
            'name': self._name,
            'academic_rank': self._academic_rank,
            'bio': self._bio,
            'image_link': self._image_link,
            'courses': self._courses,
            'total_rating': self._total_rating,
            'rates_count': self._rates_count
        }

    def __repr__(self) -> str:
        return str(self.__dict__())

    def __eq__(self, other) -> bool:
        if not isinstance(other, Teacher):
            return False

        return \
            self._name == other._name \
            and self._bio == other._bio \
            and self._academic_rank == other._academic_rank \
            and self._courses == other._courses
