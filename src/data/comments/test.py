from data.comments.comments import CommentsDB


def run() -> None:
    db = CommentsDB()
    db.create_comment(teacher_id=0, author='admin', text='Отлично')
    db.create_comment(teacher_id=0, author='admin', text='Великолепно')
    db.create_comment(teacher_id=2, author='admin', text='Мда...')
    db.create_comment(teacher_id=2, author='admin', text='Кошмарно')

    for comment in db.get_comments_about_teacher(teacher_id=0):
        comment.add_rate(-1)
        db.update_comment(comment)

    for comment in db.get_comments_about_teacher(teacher_id=2):
        comment.add_rate(+1)
        db.update_comment(comment)

    for comment in db.get_comments_about_teacher(teacher_id=2):
        comment.add_rate(+1)
        db.update_comment(comment)

    for teacher_id in [0, 2]:
        print(teacher_id)
        for comment in db.get_comments_about_teacher(teacher_id=teacher_id):
            print(comment)


if __name__ == '__main__':
    run()
