

class Comment:

    def __init__(
            self,
            _id: int,
            author: str,
            create_time: int,
            text: str,
            teacher_id: int,
            total_rating: int = 0):
        self._id: int = _id
        self._author: str = author
        self._create_time: int = create_time
        self._text: str = text
        self._teacher_id: int = teacher_id
        self._total_rating: int = total_rating

    def get_author(self) -> str:
        return self._author

    def get_create_time(self) -> int:
        return self._create_time

    def get_text(self) -> str:
        return self._text

    def get_rating(self) -> int:
        return self._total_rating

    def get_id(self) -> int:
        return self._id

    def get_teacher_id(self) -> int:
        return self._teacher_id

    def add_rate(self, rate: int) -> None:
        self._total_rating += rate

    def __dict__(self) -> dict:
        return {
            'id': self._id,
            'author': self._author,
            'create_time': self._create_time,
            'text': self._text,
            'teacher_id': self._teacher_id,
            'total_rating': self._total_rating
        }

    def __repr__(self) -> str:
        return str(self.__dict__())
