import json
import os
import time
from typing import List, Dict, Optional

from data.comments.comment import Comment


class CommentsDB:
    _DATA_FILE_NAME: str = 'db/comments.json'

    @staticmethod
    def _load_comment(c: dict) -> Comment:
        return Comment(
            _id=c['id'],
            author=c['author'],
            create_time=c['create_time'],
            text=c['text'],
            teacher_id=c['teacher_id'],
            total_rating=c['total_rating'])

    def __init__(self):
        comments: List[dict]
        try:
            with open(CommentsDB._DATA_FILE_NAME, 'r') as comments_file:
                comments = json.load(comments_file)
        except FileNotFoundError:
            os.makedirs(os.path.dirname(CommentsDB._DATA_FILE_NAME), exist_ok=True)
            with open(CommentsDB._DATA_FILE_NAME, 'w') as comments_file:
                comments_file.write('[]')
            comments = []

        self._comments: Dict[int, Dict[int, Comment]] = {}
        max_id: int = 0

        for comment_json in comments:
            comment: Comment = CommentsDB._load_comment(comment_json)
            if comment.get_teacher_id() not in self._comments:
                self._comments[comment.get_teacher_id()] = {}
            self._comments[comment.get_teacher_id()][comment.get_id()] = comment
            if comment.get_id() > max_id:
                max_id = comment.get_id()

        self._next_id = max_id + 1

    def get_comment(self, comment_id: int) -> Optional[Comment]:
        for teacher_id in self._comments:
            teacher_comments: Dict[int, Comment] = self._comments[teacher_id]
            if comment_id in teacher_comments:
                return teacher_comments[comment_id]

        return None

    def get_comments_about_teacher(self, teacher_id: int) -> List[Comment]:
        if teacher_id not in self._comments:
            return []

        return list(map(
            lambda comment_id: CommentsDB._load_comment(self._comments[teacher_id][comment_id].__dict__()),
            self._comments[teacher_id]))

    def create_comment(self, teacher_id: int, author: str, text: str) -> Comment:
        _id: int = self._next_id
        self._next_id += 1
        comment = Comment(_id=_id, teacher_id=teacher_id, author=author, text=text, create_time=int(time.time()))
        if teacher_id not in self._comments:
            self._comments[teacher_id] = {}
        self._comments[teacher_id][comment.get_id()] = comment
        self._store()
        return CommentsDB._load_comment(comment.__dict__())

    def update_comment(self, comment: Comment) -> None:
        self._comments[comment.get_teacher_id()][comment.get_id()] = comment
        self._store()

    def delete_comment(self, comment_id: int) -> Optional[str]:
        for teacher_id in self._comments:
            teacher_comments: Dict[int, Comment] = self._comments[teacher_id]
            print(teacher_id, teacher_comments, comment_id in teacher_comments)
            if comment_id in teacher_comments:
                comment: Comment = teacher_comments.pop(comment_id)
                self._store()
                return comment.get_author()

        return None

    def _store(self) -> None:
        os.makedirs(os.path.dirname(CommentsDB._DATA_FILE_NAME), exist_ok=True)
        with open(CommentsDB._DATA_FILE_NAME, 'w') as comments_file:
            all_comments: List[dict] = []
            for teacher_id in self._comments:
                all_comments.extend(
                    list(map(
                        lambda comment_id: self._comments[teacher_id][comment_id].__dict__(),
                        self._comments[teacher_id])))
            json.dump(obj=all_comments, fp=comments_file)
