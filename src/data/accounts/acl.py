

class AccessControlList:

    def __init__(self, admin: bool = False, visible: bool = True):
        self._admin: bool = admin
        self._visible: bool = visible

    def is_admin(self) -> bool:
        return self._admin

    def comments_visible(self) -> bool:
        return self._visible

    def __dict__(self) -> dict:
        return {
            'admin': self._admin,
            'visible': self._visible
        }

    def __repr__(self) -> str:
        return str(self.__dict__())
