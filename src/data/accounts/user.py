from typing import List

from data.accounts.acl import AccessControlList
from data.accounts.rate import Rate


class User:

    def __init__(
            self,
            name: str,
            password_hash: int,
            acl: AccessControlList,
            create_time: int,
            comments_count: int = 0,
            rates: List[Rate] = None):
        if rates is None:
            rates = []
        self._name: str = name
        self._password_hash: int = password_hash
        self._acl: AccessControlList = acl
        self._create_time: int = create_time
        self._comments_count: int = comments_count
        self._rates: List[Rate] = rates

    def update_acl(self, acl: AccessControlList) -> None:
        self._acl = acl

    def get_name(self) -> str:
        return self._name

    def get_password_hash(self) -> int:
        return self._password_hash

    def get_rates(self) -> List[Rate]:
        return self._rates

    def get_create_time(self) -> int:
        return self._create_time

    def get_acl(self) -> AccessControlList:
        return self._acl

    def add_rate(self, rate: Rate) -> None:
        self._rates.append(rate)

    def increment_comments_count(self) -> None:
        self._comments_count += 1

    def decrement_comments_count(self) -> None:
        print('decrement comments count for ' + self.get_name())
        self._comments_count -= 1

    def __dict__(self) -> dict:
        return {
            'name': self._name,
            'password_hash': self._password_hash,
            'acl': self._acl.__dict__(),
            'create_time': self._create_time,
            'comments_count': self._comments_count,
            'rates': list(map(lambda it: it.__dict__(), self._rates))
        }

    def __repr__(self) -> str:
        return str(self.__dict__())
