import json
import os
from typing import List, Dict, Optional
import time

from data.accounts.rate import Rate
from data.accounts.user import User
from data.accounts.acl import AccessControlList


class AccountsDB:
    _DATA_FILE_NAME: str = 'db/accounts.json'

    @staticmethod
    def _load_user(u: dict) -> User:
        return User(
            name=u['name'],
            password_hash=u['password_hash'],
            acl=AccessControlList(
                admin=u['acl']['admin'],
                visible=u['acl']['visible']),
            create_time=u['create_time'],
            comments_count=u['comments_count'],
            rates=list(map(lambda r: Rate(_type=r['type'], _id=r['id'], rate=r['rate']), u['rates'])))

    def __init__(self):
        accounts: List[dict]
        try:
            with open(AccountsDB._DATA_FILE_NAME, 'r') as accounts_file:
                accounts = json.load(accounts_file)
        except FileNotFoundError:
            os.makedirs(os.path.dirname(AccountsDB._DATA_FILE_NAME), exist_ok=True)
            with open(AccountsDB._DATA_FILE_NAME, 'w') as accounts_file:
                accounts_file.write('[]')
            accounts = []
        self._users: Dict[str, User] = {}
        for user_json in accounts:
            user: User = AccountsDB._load_user(user_json)
            self._users[user.get_name()] = user

    def get_user(self, name: str) -> Optional[User]:
        if name not in self._users:
            return None

        return AccountsDB._load_user(self._users[name].__dict__())

    def update_user(self, user: User) -> None:
        self._users[user.get_name()] = user
        self._store()

    def create_user(self, name: str, password_hash: int, acl: AccessControlList) -> Optional[User]:
        if name in self._users:
            return None

        self._users[name] = User(
            name=name,
            password_hash=password_hash,
            acl=acl,
            create_time=int(time.time()))
        self._store()
        return self.get_user(name)

    def _store(self) -> None:
        os.makedirs(os.path.dirname(AccountsDB._DATA_FILE_NAME), exist_ok=True)
        with open(AccountsDB._DATA_FILE_NAME, 'w') as accounts_file:
            json.dump(obj=list(map(lambda user_name: self._users[user_name].__dict__(), self._users)), fp=accounts_file)
