class Rate:
    TEACHER: str = 't'
    COMMENT: str = 'c'

    def __init__(self, _type: str, _id: int, rate: int):
        self._type: str = _type
        self._id: int = _id
        self._rate: int = rate

    def get_type(self) -> str:
        return self._type

    def get_id(self) -> int:
        return self._id

    def get_rate(self) -> int:
        return self._rate

    def __dict__(self) -> dict:
        return {
            'type': self._type,
            'id': self._id,
            'rate': self._rate
        }

    def __repr__(self) -> str:
        return str(self.__dict__())
