import json
import os
from typing import Dict


class PasswordsDB:
    _DATA_FILE_NAME: str = 'db/passwords.json'

    @staticmethod
    def hash(password: str) -> int:
        result = 5381

        for c in password:
            result = result * 33 + ord(c)

        return result

    def __init__(self):
        passwords: dict
        try:
            with open(PasswordsDB._DATA_FILE_NAME, 'r') as passwords_file:
                passwords = json.load(passwords_file)
        except FileNotFoundError:
            os.makedirs(os.path.dirname(PasswordsDB._DATA_FILE_NAME), exist_ok=True)
            with open(PasswordsDB._DATA_FILE_NAME, 'w') as passwords_file:
                passwords_file.write('{}')
            passwords = {}

        self._passwords: Dict[str, str] = passwords

    def set_password(self, user: str, password: str) -> None:
        self._passwords[user] = password
        self._store()

    def check_password(self, user: str, password: str) -> bool:
        if user not in self._passwords:
            return False

        return self._passwords[user] == password

    def delete_password(self, user: str) -> None:
        _ = self._passwords.pop(user)
        self._store()

    def _store(self) -> None:
        os.makedirs(os.path.dirname(PasswordsDB._DATA_FILE_NAME), exist_ok=True)
        with open(PasswordsDB._DATA_FILE_NAME, 'w') as passwords_file:
            json.dump(obj=self._passwords, fp=passwords_file)
